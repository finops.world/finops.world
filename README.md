![FinOps.World](./finops.world-logo-darkM.png)

# FinOps.World

**Finops.world** est le 1er référentiel Open Source de bonnes pratiques FinOps.  
Initié par [Timspirit](https://timspirit.fr), le référentiel est désormais rédigé de manière collégiale.

Sa dernière version est `Winter2019` (7dc5bccc) et est disponible également sur le site https://finops.world  
Vous pouvez également nous retrouver à l'occasion des [meetup Finops](https://www.meetup.com/fr-FR/Meetup-Cloud-FinOps-Paris/).

## Contribution
Si vous souhaitez contribuer au référentiel, vous pouvez demander à joindre le projet dans Gitlab ou poouvez nous contacter à contact@finops.world   
Voir également le fichier [Contributing](./CONTRIBUTING.md) pour les détails.

## Licensing
**FinOps.world** est publié sous licence [Creative Commons CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.fr).

Il peut être librement utilisé et modifié sous réserve de citer l’auteur et de repartager de la même manière. L’usage commercial est autorisé.

Voir également le fichier [Licensing](./LICENSE).

# L'organisation du référentiel FinOps.World

Le référentiel est organisé autour de 3 niveaux de maturité:

| Maturité | Description |
| -------- | ----------- |
| Niveau 1 : Démarrage | Reprendre le contrôle de sa facture Cloud |
| Niveau 2 : Pérennisation | Pérenniser la maîtrise des dépenses au sein de l'organisation |
| Niveau 3 : Généralisation | Instaurer la sobriété numérique comme pilier de l'organisation |

associé à 6 étapes du cycle de vie:
 - Stratégie
 - Gouvernance
 - Build
 - Run
 - Optimisation
 - Veille & Expertise

ainsi que 10 rôles clefs.

# Version
La dernière Version disponible sur le site est la version `Winter 2019`
