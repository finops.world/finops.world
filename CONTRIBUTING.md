# Contributors
Everyone can ask to be part of the project and request access through GitLab.  
For any further information, mail [contact@finops.world](mailto:contact@finops.world).

# Language (🇫🇷|🇺🇸)

Today:
- French is the by-default language for practice's definitions in the `master` branch.
- French is also the by-default language for updating the current practice's definitions
- In Merge Requests or Issued, you can comment in French or English

Some files, including this one, are written in English to be understandable by most people.

Once a release of the framework is ready to be published on the website (finops.world), a translation into English is made by the team.  
The tag name is then postfixed by "En", e.g.:
- `Winter2019`, and its english version `Winter2019En` 

# Branching & contributing

`MASTER` is protected and no one except the owner is allowed to pushed into it.  
Pushing into `master` is reserved to files in the root directory an not to practices. Files allowed to be pushed to `master` includes:

- `README.md`
- `CONTRIBUTING.md`
- `LICENSE`
- `scripts/*`

Validated contributors must suggest updates **for existing practices** on dedicated branches :

- for individual contributor, branch names format is `<contributorname>/<branchname>`
- for companies, branch names format is `<company>/<branchname>`

If you intend to focus on a specific practice, include the ID into the branch (For example : `timspirit/2G3-update`).

Branches are then merged back to `MASTER` through Merge Requests (MR) after peer validation and reviews.  
**Squashing the commit history when merging is encouraged**.

Keep branche's content light to ease MR process and reviews. Create multiple MR if needed.  
⚠️ **Don't create long living branches**.

If you think we might **create/change/delete/merge practices**, please open an issue and explain why. In this case, prefix the issue by the 🆕 emoji.

## Resources

Docs and pictures are uploaded in the `Resources` directory in each domain.  
Name those resources with a understandable name separated with dashes in order to ease SEO after integration in the website.  
For example : `tags-definition-billing.png`

## Practice template

A template is defined through #2.  
Please respect this convention for any updates.  
Suggest any updates to the template in comments in #2.

# Tagging

Releases integrated to the website are tagged in `MASTER` branch.  
First release/tag is `Winter2019` ([here](https://gitlab.com/finops.world/finops.world/-/tree/Winter2019)).