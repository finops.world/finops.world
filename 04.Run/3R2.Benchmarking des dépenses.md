# Benchmarking des dépenses

|  |  |
| --- | --- |
| **Niveau** | 3-Généralisation |
| **Catégorie** | Run |
| **ID** | 3R2 |
| **Rôle(s) Clé(s)** | Lead DevOps |

## Objectifs
Comparer les dépenses à plusieurs niveaux de benchmarking

## Bénéfices
Augmenter la capacité d'investigation dans l'optimisation des dépenses

# Description
Le benchmarking sert avant tout à se poser les bonnes questions ! Il peut se faire à différents niveaux.

<div align="center">
![Illustration des niveaux de benchmarking](./Resources/niveau-benchmarking.png)

*Illustration des niveaux de benchmarking*
</div>

Illustration des niveaux de benchmarking
Selon les cas on peut vouloir comparer ses dépenses au niveau :

- Du marché en général
- D’autres industries du même secteur
- D’autres Business Units
- D’autres solutions
- Et pourquoi d'autres sous-services...

Et on peut bien sûr vouloir comparer les dépenses par solution par catégorie ou tout autre niveau et faire des comparaisons en valeur ou en pourcentage.

Voici un exemple issu du [TBM Council](https://www.tbmcouncil.org/) de comparaison par IT Tower, terme utilisé dans le Technical Business Management pour désigner les principaux postes de dépenses IT.

<div align="center">
![Exemple de comparaison des IT Towers](./Resources/it-towers-tbm.png)

*Exemple de comparaison des IT Towers avec le benchmark (issu du TBM Council)*
</div>

On compare chaque poste à un benchmark (quel qu’il soit) en pourcentage.

Voici un autre exemple au niveau solution cette fois, on l’on compare deux solutions A et B selon différents angles :

- le coût par transaction,
- en séparant compute et storage
- et en regardant les coûts totaux mis au regard de la moyenne de transactions et de users…


<div align="center">
![Exemple de comparaison entre deux solutions](./Resources/comparaison-1.png)

*Exemple de comparaison entre deux solutions*
</div>

<div align="center">
![Exemple de comparaison de deux solutions (sous un angle différent)](./Resources/comparaison-2.png)

*Exemple de comparaison de deux solutions (sous un angle différent)*
</div>

L’idée donc est de trouver les bons niveaux de comparaison et les bonnes données à comparer pour en tirer des conclusions…
