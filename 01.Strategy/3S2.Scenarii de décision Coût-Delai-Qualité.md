# Scenarii de décision Coût-Delai-Qualité

|  |  |
| --- | --- |
| **Niveau** | 3-Généralisation |
| **Catégorie** | Stratégie |
| **ID** | 3S2 |
| **Rôle(s) Clé(s)** | Lead FinOps |

## Objectifs
Mettre en place une politique d'arbitrage basée sur des critères factuels d'impact

## Bénéfices
Faciliter les arbitrages

# Description
C’est un objectif majeur d’une démarche FinOps globale : Aider à réaliser le bon compromis entre les 3 dimensions : Coût, Rapidité, Qualité.

<div align="center">
![Illustration des dimensions coûts-rapidité-qualité](./Resources/dimension.cqd.png)

*Illustration des dimensions coûts-rapidité-qualité*
</div>

Pour être en mesure de réaliser les bons choix, il peut être utile de mettre en place un scoring des opportunités d’optimisation en se basant sur les impacts pondérés incluant :

- Les Coûts
- Le Time-to-Market
- La qualité
- La fiabilité
- La résilience
- L’impact business

Prenons un exemple avec 3 scenarii comparés :


| IMPACTS | POIDS | SC.1 | SC.2 | SC.3 |
| --- | --- | --- | --- | --- |
| Coûts | 20 | 4 | 2 | 1 |
| Qualité | 10 | 1 | -1 | 0 |
| Fiabilité | 20 | 0 | 1 | 2 |
| Résilience | 10 | 0 | 1 | 2 |
| Business | 15 | -1 | 0 | 1 |
| TOTAL | 100 | 50 | 85 | 145 |

En comparant chaque score de chaque dimension on peut voir par exemple que le scenario A représente le meilleur impact sur les coûts et en time-to-market.

<div align="center">
![Graphique Araignée comparant les solutions selon les dimensions](./Resources/analyse.dimension.png)

*Graphique Araignée comparant les solutions selon les dimensions*
</div>

Mais en calculant les scores pondérés on se rend compte que, dans notre cas précis, le scenario C est le meilleur, en terme de score total mais aussi parce qu'il n’a que des impacts positifs ou neutres.

<div align="center">
![Graphique combiné comparant les solutions par dimension et avec leur score combiné](./Resources/analyse.dimension.2.png)

*Graphique combiné comparant les solutions par dimension et avec leur score combiné*
</div>

Avec une représentation de ce type, on peut comparer pour chaque dimension mais aussi le total. C’est aussi une démonstration qu’il faut rester vigilant à la façon de présenter les informations, pour être en mesure de faire les bons choix adaptés à l’entreprise.
