#!/usr/local/bin/python

import csv

PracticesList = "practices.list"
OutputFile = ""
Line = 0


LevelList = {'1': '1-Démarrage ',
             '2': '2-Pérennisation',
             '3': '3-Généralisation'}


with open("./"+PracticesList, 'r', encoding='utf8') as Practices:
    reader = csv.reader(Practices, delimiter=';')

    for p in reader:
        Line = Line + 1

        if p[0].startswith("//"):
            continue

        print("Line: "+str(Line)+": "+str(p))

        Level = p[0]
        Domain = p[1]
        Order = p[2]
        Role = p[3]
        Label = p[4]

        OutputFile = "# " + Label + "\n\n"
        OutputFile = OutputFile + "|  |  |\n| --- | --- |\n"
        OutputFile = OutputFile + "| **Niveau** | " + LevelList[Level] + " |\n"
        OutputFile = OutputFile + "| **Catégorie** | " + Domain + " |\n"
        OutputFile = OutputFile + "| **ID** | " + Level + Domain[0] + Order + " |\n"
        OutputFile = OutputFile + "| **Rôle(s) Clé(s)** | " + Role + " |\n\n"

        OutputFile = OutputFile + "## Objectifs\n\n\n"

        OutputFile = OutputFile + "## Bénéfices\n\n\n"

        OutputFile = OutputFile + "# Description\n\n\n"

        # print(OutputFile)

        with open("md/"+Level+Domain[0]+Order+"."+Label.replace("/", "")+".md", 'w') as md:
            md.write(OutputFile)
