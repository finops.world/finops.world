# Budgeting & Forecasting

|  |  |
| --- | --- |
| **Niveau** | 3-Généralisation |
| **Catégorie** | Gouvernance |
| **ID** | 3G1 |
| **Rôle(s) Clé(s)** | Lead FinOps |

## Objectifs
Mettre en place un budget IT solide ainsi qu'un suivi et une mise à jour réguliers

## Bénéfices
Rendre le budget IT plus transparent et plus itératif en améliorant la communication entre IT et business

# Description
Un certain nombre d'activités sont conseillées pour améliorer le budget IT et son suivi :

<div align="center">
![Illustration des actions initiales à mener](./Resources/budget-action-initiale.png)

*Illustration des actions initiales à mener*
</div>

## Business Planning
Le budget IT top down fait à la suite du business planning est le propre d'une organisation où l'IT est considérée comme un centre de coûts. Pour qu'elle (re)devienne un business driver, son budget doit être fait pendant la phase de business planning en intégrant la segmentation définie avec les métiers mais surtout en basant les coûts sur les estimations de consommation des services par les métiers et du coût unitaire de ces mêmes services.

## Baseline & Zero-based
On peut mixer les approches :

- L’approche baseline consistant à se baser sur les dépenses de l’année passée
- Mais aussi sur l’approche dite de « Zero-based budgeting » qui consiste à revoir chaque euro du budget en se redemandant si cela correspond bien au besoin et en donnant une visibilité accrue sur les drivers des coûts et les targets business.

## Monthly Reporting
Allant de paire avec cette approche de budgeting vient le reporting régulier (mensuel à minima) à instaurer pour chaque application, service et/ou sous-périmètre selon le contexte :

- en comparant le réel au budget,
- en analysant les raisons des variances
- en ré-estimant la consommation des services par les métiers et donc leur coût unitaire afin d’anticiper les dépassements de budget.

<div align="center">
![Exemple de reporting mensuel](./Resources/ex-reporting.png)

*Exemple de reporting mensuel*
</div>

## Reforcasting
Enfin, le reforcasting est un exercice à ne pas négliger. Pourquoi attendre la fin de l’année fiscale si l’on sait que les tendances précédentes et les ré-estimations indiquent que le budget n’est plus d’actualité ?


