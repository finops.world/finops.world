# Contrôle des objectifs

|  |  |
| --- | --- |
| **Niveau** | 3-Généralisation |
| **Catégorie** | Gouvernance |
| **ID** | 3G2 |
| **Rôle(s) Clé(s)** | Lead FinOps |

## Objectifs
Mettre en place un suivi des objectifs fixés afin de contrôler leur atteinte

## Bénéfices
Etre capable de capitaliser sur les succès et de réajuster en cas de problème

# Description
Qu'il s'agisse d'objectifs propres à une application ou plus globaux, il faut mettre en place un suivi afin de les contrôler. L'idée est donc de réaliser un reporting FinOps complet incluant :

- **Les indicateurs IT** de mesure d’efficacité
- **Les indicateurs métiers** de mesure d’impact des éléments qui affectent les marges de l’entreprise

<div align="center">
![Exemple de suivi d'objectifs IT](./Resources/it-efficiency.png)

*Exemple de suivi d'objectifs IT*
</div>

<div align="center">
![Exemple de suivi d'objectifs métiers](./Resources/business-unit-cost.png)

*Exemple de suivi d'objectifs métiers*
</div>

On peut voir dans les exemples ci-dessus une hausse globale des coûts globaux mais :

- D’un côté une baisse des coûts de compute et de stockage
- De l’autre une baisse des coûts par transaction et par user

Cela signifie qu’on est bon ! Si les coûts unitaires baissent mais que les coûts globaux augmentent c’est que les volumes métiers augmentent et donc les marges !
