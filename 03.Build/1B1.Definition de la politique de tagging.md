
# Définition de la politique de tagging


|  |  |
| --- | --- |
| **Niveau** | 1. Démarrage |
| **Catégorie** | Build |
| **ID** | 1B1 |
| **Rôle(s) Clé(s)** | Lead Architect, Lead FinOps |
| **Mots Clés** | tag, tagging, policy |

## Objectifs
Définir la politique de tagging initiale afin de permettre l’analyse des dépenses

## Bénéfices
Faciliter le reporting et l'investigation sur les dépenses

# Description
Mettre en place le tagging est l'une des toutes premières actions à mener. Cela consiste à associer des mots-clés aux ressources Cloud afin de pouvoir les suivre et les identifier tout au long de leur cycle de vie. Les tags sont cruciaux pour les tâches de recherche et d'attribution mais également pour la gestion des dépenses et l’optimisation des coûts.

## Tags Standards

Voici des exemples de tags qu'il est possible de mettre en œuvre. 

|Tags|Description|
| --- | --- |
| **Name** | Identification de la ressource individuelle |
| **Application** | Identification de l’application à laquelle est rattachée la ressource |
| **Role** | Fonction de la ressource dans l’application |
| **Cluster** | Cluster auquel la ressource appartient |
| **Environment** | Dev test, staging, production, etc |
| **Version** | Version de la ressource |
| **Owner** | Propriétaire de la ressource |
| **Business Unit** | Division responsable budgétairement de la ressource |
| **Cost Center** | Centre de Coûts comptable associé à la ressource |
| **Customer** | Client spécifique |
| **Project** | Projet spécifique |
| **Confidentiality** | Niveau de confidentialité |
| **Project** | Projet spécifique |
| **Compliance** | Ensemble d’exigences de conformité applicables |
| **Country** | Pays consommateur(s) de la ressource |


Il est primordial de déterminer quels tags sont obligatoires dans l’organisation concernée et lesquels sont utiles mais facultatifs. Quel que soit le schéma, il est important de le centraliser dans un document de référence, de **s’assurer de son application** et de son maintien à jour. 

Il convient de veiller à ne pas imposer trop de tags : si le tagging est perçu comme une perte de temps, ou si les équipes ne sont pas convaincues de leur utilité, alors la politique de tagging pour le finops risque d'être ignorée ou bâclée.

Les Cloud Providers proposent des outils, **tagging policies**, qui facilitent la gestion des tags. Il faut garder en tête que le nombre de tags applicables sur une ressource est limité. Cette limite peut varier d'un type de ressource à un autre et d'un Cloud Provider à un autre. De plus, les tags peuvent être également être utilisés par d'autres populations :

- les **Ops**, par exemple pour définir l'arrêt ou le démarrage ou définir la durée de rétention de la sauvegarde,
- la **sécurité** pour définir le chiffrement ou l'isolation de la ressource 
- les développeurs, pour gérer depuis leur application des ressources du cloud sous-jacent. 

Une coordination globale est donc indispensable pour définir une politique de tagging pertinente et équilibrée dans les limites imposées.

## Particularités par CSP

### AWS

Afin que les tags techniques (**Ops** ou **Sécu**) ne viennent pas polluer les données **FinOps**, AWS permet d'identifier les **Cost allocation tags** dans le **Billing dashboard**


# Pratiques associées

[2B1. Gestion de la politique de tagging automatisée](/03.Build/2B1.Gestion de la politique de tagging automatisée.md)
[2G3. Refacturation](/02.Governance/2G3.Refacturation.md)
