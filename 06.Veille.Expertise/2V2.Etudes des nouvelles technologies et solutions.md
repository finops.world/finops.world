# Etudes des nouvelles technologies et solutions

|  |  |
| --- | --- |
| **Niveau** | 2-Pérennistion |
| **Catégorie** | Veille & Expertise |
| **ID** | 2V2 |
| **Rôle(s) Clé(s)** | Lead R&D |

## Objectifs
Faire de la veille technologique pour trouver de nouvelles opportunités d'amélioration

## Bénéfices
Améliorer l'efficacité IT par l'innovation

# Description
Bien que souvent, la bonne approche soit de partir d'une idée et trouver ensuite la technologie adaptée, l'innovation passe parfois par le fait qu'une nouvelle technologie peut donner des idées de nouveaux usages, de nouvelles possibilités... C’est à l’image du Cloud dont les innombrables possibilités ont créé de nouvelles « value propositions » pour les produits.

Cela peut passer par :

- de nouveaux composants IaaS
- de nouveaux services PaaS
- de nouvelles solutions SaaS
- de nouvelles régions
- de nouvelles options d'achat
- des architectures serverless
- des solutions de conteneurisation
- l'utilisation de spot instances

....
Mais il ne suffit pas d’avoir lu un blog pour identifier les optimisations de demain : Il faut prouver le concept et réaliser des documents de synthèse (cf: 2V1 - Identification des opportunités de refactoring).

## Conteneurs & microservices
L'utilisation des conteneurs doit être considérée dans l'optimisation des coûts. Si l'on prend l'exemple d'AWS, il n'y a aucun coût supplémentaire pour AWS Elastic Container Service (ECS) : On paie simplement pour les instances EC2 sous-jacentes et les volumes EBS associés. Cela permet par exemple d'utiliser des conteneurs Docker pour consolider plusieurs petites instances sous-utilisées. Mais on peut gagner encore plus en allant vers des microservices "serverless", en utilisant par exemple des fonctions Lambda qui ne seront appelées par API qu'en cas de besoin et payées au temps d'exécution.

## Replatforming de bases de données
Passer à l'open source, comme PostgreSQL, MySQL ou MariaDB, ou en migrant vers Aurora d'AWS, on peut faire des économies de licences importantes !

Bien entendu, on n'envisagera pas la migration massive de toutes les bases de données de l'entreprise mais on peut envisager des refactoring pragmatiques en séparant certaines fonctions.

## Utilisation de flottes ponctuelles
Les flottes ponctuelles (spot instances) permettent de mettre à moindre coût (meilleur prix du marché) une grande flotte de "workers" pour effectuer des tâches mineures pouvant être arrêtées à tout moment. Cela peut servir par exemple pour effectuer en tâche de fond des redimensionnements d'images pour un site web.

## Du IaaS pur vers le SaaS / Serverless

<div align="center">
![Illustration du chemin d'optimisation entre le IaaS pur et le SaaS / PaaS / Serverless](./Resources/2V2.png)

*Illustration du chemin d'optimisation entre le IaaS pur et le SaaS / PaaS / Serverless*
</div>
